$('.js-item-tab').on('click', function () {
    $(this).addClass('is-active-tab').siblings().removeClass('is-active-tab');
    $('.js-content-cpec')
                        .hide()
                        .eq($(this).index())
                        .addClass('is-active-content-cpec')
                        .fadeIn()
                        .siblings()
                        .removeClass('is-active-content-cpec')
                        .fadeOut();
})


// $('.js-addition-hend').on('click', function(event) {
//     event.preventDefault();
//     $(this).toggleClass('is-active-tab');
//     $('.js-addition-show').fadeToggle();
// });

// $(document).on('click', function (e) {
//     if ($(e.target).closest('.b-pick-specialist-tabs__item--hend').length) {
//         return;
//     }

//     $('.js-addition-hend').removeClass('is-active-tab');
//     $('.js-addition-show').fadeOut();
// });


if ($(window).width() <= 1330) {
    $('.js-addition-hend')
                        .find('.b-pick-specialist-tabs__addition')
                        .addClass('remodal')
                        .attr('data-remodal-id', 'addition-mob');
    
    $('[data-remodal-id=addition-mob]').remodal();
}