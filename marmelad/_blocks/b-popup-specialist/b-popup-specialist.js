$('.b-popup-specialist__tab-switches').on('click', '.js-btn-switch:not(.is-active-btn)', function () {
    $(this).addClass('is-active-btn').siblings().removeClass('is-active-btn');
    $('.js-tab-content').removeClass('is-active-content').hide().eq($(this).index()).fadeIn();
});