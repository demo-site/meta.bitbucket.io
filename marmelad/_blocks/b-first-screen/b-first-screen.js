// $('.js-addition-btn').on('hover', function() {
//     $('.js-addition').show();
// });

// $(document).on('click', function (e) {
//      if ($(e.target).closest('.b-first-screen__bot-name').length) {
//          return;
//    }

//     $('.js-addition').fadeOut();
// });

$('.js-addition-btn').hover(function() {
    $('.js-addition').fadeIn(100);
},
function() {
    $('.js-addition').fadeOut(100);
}
);

$('.js-slider-cpec').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    fade: true,
    asNavFor: '.js-slider-cpec-thumb'
});
$('.js-slider-cpec-thumb').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.js-slider-cpec',
    arrows: false,
    autoplay: true,
    dots: false,
    // centerMode: true,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 1023,
            settings: {
                slidesToShow: 5
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 4
            }
        },
        {
            breakpoint: 440,
            settings: {
                slidesToShow: 3
            }
        }
    ]
});

var videoModal = $('[data-remodal-id=video]').remodal();

$('.b-first-screen__play-prev').on('click', function (event) {
    event.preventDefault();
    var $this = $(this),
        videoSrc = $this.attr('href'),
        inRemodalVideo = $this.closest('.profile-spec').find('.b-first-screen__video');
    
    inRemodalVideo.find('iframe').attr('src', videoSrc);
    videoModal.open();
});
$(document).on('closed', '[data-remodal-id=video]', function (e) {
    $('[data-remodal-id="video"] iframe').attr('src', '');
});