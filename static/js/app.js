"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  }; // $('.js-addition-btn').on('hover', function() {
  //     $('.js-addition').show();
  // });
  // $(document).on('click', function (e) {
  //      if ($(e.target).closest('.b-first-screen__bot-name').length) {
  //          return;
  //    }
  //     $('.js-addition').fadeOut();
  // });


  $('.js-addition-btn').hover(function () {
    $('.js-addition').fadeIn(100);
  }, function () {
    $('.js-addition').fadeOut(100);
  });
  $('.js-slider-cpec').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    fade: true,
    asNavFor: '.js-slider-cpec-thumb'
  });
  $('.js-slider-cpec-thumb').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.js-slider-cpec',
    arrows: false,
    autoplay: true,
    dots: false,
    // centerMode: true,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1023,
      settings: {
        slidesToShow: 5
      }
    }, {
      breakpoint: 640,
      settings: {
        slidesToShow: 4
      }
    }, {
      breakpoint: 440,
      settings: {
        slidesToShow: 3
      }
    }]
  });
  var videoModal = $('[data-remodal-id=video]').remodal();
  $('.b-first-screen__play-prev').on('click', function (event) {
    event.preventDefault();
    var $this = $(this),
        videoSrc = $this.attr('href'),
        inRemodalVideo = $this.closest('.profile-spec').find('.b-first-screen__video');
    inRemodalVideo.find('iframe').attr('src', videoSrc);
    videoModal.open();
  });
  $(document).on('closed', '[data-remodal-id=video]', function (e) {
    $('[data-remodal-id="video"] iframe').attr('src', '');
  });
  $('.js-item-tab').on('click', function () {
    $(this).addClass('is-active-tab').siblings().removeClass('is-active-tab');
    $('.js-content-cpec').hide().eq($(this).index()).addClass('is-active-content-cpec').fadeIn().siblings().removeClass('is-active-content-cpec').fadeOut();
  }); // $('.js-addition-hend').on('click', function(event) {
  //     event.preventDefault();
  //     $(this).toggleClass('is-active-tab');
  //     $('.js-addition-show').fadeToggle();
  // });
  // $(document).on('click', function (e) {
  //     if ($(e.target).closest('.b-pick-specialist-tabs__item--hend').length) {
  //         return;
  //     }
  //     $('.js-addition-hend').removeClass('is-active-tab');
  //     $('.js-addition-show').fadeOut();
  // });

  if ($(window).width() <= 1330) {
    $('.js-addition-hend').find('.b-pick-specialist-tabs__addition').addClass('remodal').attr('data-remodal-id', 'addition-mob');
    $('[data-remodal-id=addition-mob]').remodal();
  }

  $('.b-popup-specialist__tab-switches').on('click', '.js-btn-switch:not(.is-active-btn)', function () {
    $(this).addClass('is-active-btn').siblings().removeClass('is-active-btn');
    $('.js-tab-content').removeClass('is-active-content').hide().eq($(this).index()).fadeIn();
  });

  if ($.exists('.app--bProfile-type2')) {
    var addDisable = function addDisable() {
      var indexItemWrap = 1;
      $('.b-profile__item-wrap').each(function () {
        var indexRadio = 1;
        var indexCheckbox = 1;
        var checkCheckbox = 0;
        $(this).find('.b-profile__input-radio').each(function () {
          if ($('.b-profile__item-wrap:nth-child(' + indexItemWrap + ')').find('.b-profile__input-radio:nth-child(' + indexRadio + ')').find('.jq-radio').hasClass('checked')) {
            $('.b-profile__item-wrap:nth-child(' + indexItemWrap + ')').find('.b-profile__next').addClass('disabled');
          }

          indexRadio++;
        }); // $(this).find('.b-profile__input-checkbox').each(function(){
        //     if($('.b-profile__item-wrap:nth-child(' + indexItemWrap +')').find('.b-profile__input-checkbox:nth-child(' + indexCheckbox +')').find('.b-profile__checkbox-wrap').children('.jq-checkbox').hasClass('checked')){
        //         $('.b-profile__item-wrap:nth-child(' + indexItemWrap +')').find('.b-profile__next').addClass('disabled');
        //         checkCheckbox++;
        //     }
        //     // console.log(indexItemWrap);
        //     // console.log(indexCheckbox);
        //     indexCheckbox++;
        // });
        // // $('.b-profile__item-wrap:nth-child('+ indexItemWrap +')').find('.b-profile__demand-textarea')
        // // b-profile__item-field
        // if(checkCheckbox==0 && indexItemWrap == 2){
        //     // console.log(indexItemWrap);
        //     $('.b-profile__item-wrap:nth-child(' + indexItemWrap +')').find('.b-profile__next').removeClass('disabled');
        // }

        indexItemWrap++;
      });
    };

    var removeDisable = function removeDisable(itemField) {
      if (!itemField.hasClass('checked')) {
        itemField.removeClass('checked');
      }
    };

    setTimeout(function () {
      $('input').styler();
    }, 100);
    $('.b-profile__input-checkbox').on('click', function (event) {
      if ($(this).children('.b-profile__checkbox-wrap').children('.jq-checkbox').hasClass('checked')) {
        $(this).children('.b-profile__checkbox-text').toggleClass('clicked');
        $(this).toggleClass('checked');
      } else {
        $(this).children('.b-profile__checkbox-text').removeClass('clicked');
        $(this).removeClass('checked');
      }
    }); // $('.b-profile__input-radio:nth-child(1)').addClass('checked');
    // $('.b-profile__input-radio').on('click', function(event) {
    //     if($(this).children('.jq-radio').hasClass('checked')){
    //         $('.b-profile__input-radio').removeClass('checked'); 
    //         $(this).addClass('checked'); 
    //     }else {
    //     $(this).next('.b-profile__checkbox-text').removeClass('clicked');
    //     $('.b-profile__input-radio').removeClass('checked'); 
    //     }
    // });

    $('.b-profile__input-checkbox:nth-child(1)').addClass('checked');
    $('.b-profile__input-checkbox:nth-child(1)').children('.b-profile__checkbox-text').addClass('clicked');
    $('.b-profile__input-checkbox').hover(function () {
      var checkbox = $('.jq-checkbox');

      if ($(this).hasClass('checked')) {
        if (checkbox.hasClass('checked')) {// console.log('fdgdsg');
        }
      }
    });
    $('.b-profile__input-radio2').on('click', function (event) {
      if ($(this).children('.b-profile__input-radio-prac').children('.jq-radio').hasClass('checked')) {
        $('.b-profile__input-radio2').removeClass('checked');
        $('.b-profile__input-radio--not').removeClass('checked');
        $(this).addClass('checked');
      } else {
        // $(this).next('.b-profile__checkbox-text').removeClass('clicked');
        $('.b-profile__input-radio2').removeClass('checked');
      }
    });
    $('.b-profile__input-radio--not').on('click', function (event) {
      $('.b-profile__input-radio2').removeClass('checked');
    });
    $('.b-profile__checkbox-wrap span').mouseenter(function () {
      if ($(this).prev().hasClass('checked')) {
        $(this).prev().addClass('hover');
      }
    });
    $('.b-profile__checkbox-wrap span').mouseleave(function () {
      $(this).prev().removeClass('hover');
    });
    $('.b-profile__checkbox-wrap span').on('click', function (event) {
      $(this).prev().removeClass('hover');
    });
    $('.b-profile__about-check span').mouseenter(function () {
      if ($(this).prev().hasClass('checked')) {
        $(this).prev().addClass('hover');
      }
    });
    $('.b-profile__about-check label').mouseleave(function () {
      $(this).prev().removeClass('hover');
    });
    $('.b-profile__about-check label').on('click', function (event) {
      $(this).prev().removeClass('hover');
    });
    addDisable();
    $('.b-profile__item-field').on('change click', function (event) {
      var $this = $(this);
      var $thisWrap = $this.closest('.b-profile__item-wrap');
      $thisWrap.find('.b-profile__next').addClass('disabled');
      removeDisable($(this));
      addDisable();
    });
    $('.b-profile__next').on('click', function (event) {
      if ($(this).hasClass('disabled')) {
        $(this).closest('.b-profile__item-wrap').addClass('disabled');
        $('.b-profile__item-wrap').removeClass('enabled');
        $(this).closest('.b-profile__item-wrap').next().addClass('enabled');
        window.scrollTo(0, 0);
        var lines = $('.b-profile__menu-line');
        var activeItem = $(this).closest('.b-profile__item-block').find('.b-profile__item-wrap.enabled');
        var activeItemIndex = activeItem.index();
        var lineWidth = lines.outerWidth();

        if (activeItemIndex == 0) {
          lineWidth = lineWidth + 85;
          $('.b-profile__menu li:nth-child(1)').addClass('active');
        } else if (activeItemIndex == 1) {
          lineWidth = lineWidth + 30;
          $('.b-profile__menu li:nth-child(2)').addClass('active');
        } else if (activeItemIndex == 2) {
          lineWidth = lineWidth + 10;
        } else if (activeItemIndex == 3) {
          lineWidth = lineWidth + 15;
        } else if (activeItemIndex == 4) {
          lineWidth = lineWidth + 10;
        } else if (activeItemIndex == 5) {
          lineWidth = lineWidth + 265;
          $('.b-profile__menu li:nth-child(3)').addClass('active');
          $('.b-profile__menu li:nth-child(4)').addClass('active');
          $('.b-profile__menu li:nth-child(5)').addClass('active');
        } else if (activeItemIndex == 6) {
          lineWidth = lineWidth + 90;
          $('.b-profile__menu li:nth-child(6)').addClass('active');
        } else if (activeItemIndex == 7) {
          lineWidth = lineWidth + 95;
          $('.b-profile__menu li:nth-child(4)').addClass('active');
        }

        setTimeout(function () {
          lines.css('width', lineWidth + 'px');
        }, 100);
      }
    });
    $('.b-profile__prev').on('click', function (event) {
      $(this).closest('.b-profile__item-wrap').removeClass('enabled');
      $(this).closest('.b-profile__item-wrap').prev().addClass('enabled');
      var lines = $('.b-profile__menu-line');
      var activeItem = $(this).closest('.b-profile__item-block').find('.b-profile__item-wrap.enabled');
      var activeItemIndex = activeItem.index();
      var lineWidth = lines.outerWidth();
      window.scrollTo(0, 0);

      if (activeItemIndex == 0) {
        lineWidth = lineWidth - 85;
      } else if (activeItemIndex == 0) {
        lineWidth = lineWidth - 30;
        $('.b-profile__menu li:nth-child(2)').removeClass('active');
      } else if (activeItemIndex == 1) {
        lineWidth = lineWidth - 20;
      } else if (activeItemIndex == 2) {
        lineWidth = lineWidth - 15;
      } else if (activeItemIndex == 3) {
        lineWidth = lineWidth - 10;
        $('.b-profile__menu li:nth-child(3)').removeClass('active');
        $('.b-profile__menu li:nth-child(4)').removeClass('active');
        $('.b-profile__menu li:nth-child(5)').removeClass('active');
      } else if (activeItemIndex == 4) {
        lineWidth = lineWidth - 265;
        $('.b-profile__menu li:nth-child(3)').removeClass('active');
        $('.b-profile__menu li:nth-child(4)').removeClass('active');
        $('.b-profile__menu li:nth-child(5)').removeClass('active');
      } else if (activeItemIndex == 5) {
        lineWidth = lineWidth - 90;
        $('.b-profile__menu li:nth-child(6)').removeClass('active');
      } else if (activeItemIndex == 6) {
        lineWidth = lineWidth - 90;
      }

      setTimeout(function () {
        lines.css('width', lineWidth + 'px');
      }, 100);
    });
    $('.b-profile__checkbox-text .irs--flat .irs-handle').hover(function (event) {
      $('.b-profile__checkbox-text .irs--flat .irs-bar').toggleClass('hover');
      $('.b-profile__checkbox-text .irs--flat .irs-handle').toggleClass('hover'); // console.log('sdfs');
    });
    $('.b-profile__checkbox-text .irs-from, .b-profile__checkbox-text .irs-to').hover(function () {
      $('.b-profile__checkbox-text .irs--flat .irs-handle').toggleClass('hover');
      $('.b-profile__checkbox-text .irs--flat .irs-bar').toggleClass('hover');
      $('.b-profile__checkbox-text .irs--flat .to').toggleClass('active');
    });
    $('.b-profile__checkbox-text .irs-from, .b-profile__checkbox-text .irs-from').hover(function () {
      $('.b-profile__checkbox-text .irs--flat .from').toggleClass('active');
      $('.b-profile__checkbox-text .irs--flat .to').removeClass('active');
    });
    $('.b-profile__item-detail--prac').hide();
    $('.b-profile__input-radio-prac').on('click', function (event) {
      if ($(this).children('.jq-radio').hasClass('checked')) {
        $('.b-profile__item-detail--prac').slideDown();
      }
    });
    $('.b-profile__input-radio--not').on('click', function (event) {
      $('.b-profile__item-detail--prac').slideUp();
    });
    $('.b-profile__input-checkbox--last').on('click', function (event) {
      if ($(this).children('.b-profile__checkbox-wrap').children('.jq-checkbox').hasClass('checked')) {
        $(this).children('.b-profile__checkbox-text--last').children('.irs').children('.irs-handle').toggleClass('hover2');
        $(this).children('.b-profile__checkbox-text--last').children('.irs').children('.irs-bar').toggleClass('hover2');
      } else {
        $('.irs-handle').removeClass('hover2');
        $('.irs-bar').removeClass('hover2');
      }
    });
    $('.b-profile__input-checkbox--last').on('click', function (event) {
      if ($(this).find('.jq-checkbox').hasClass('checked')) {
        $(this).addClass('checked');
        $(this).find('.b-profile__checkbox-text--last').addClass('clicked');
      }
    });

    if ($('.b-profile__item-wrap--variant-choice2 .b-profile__amount-num').html() == 0) {
      $('.b-profile__item-wrap--variant-choice2 .b-profile__amount-pick').hide();
    }

    $('.b-profile__item-wrap--variant-choice2 .b-profile__item-inner').each(function () {
      var countNumVariantChoice = 0,
          $this = $(this);
      $(this).find('.b-profile__input-checkbox-circle').each(function () {
        $(this).on('click', function () {
          if ($(this).hasClass('checked')) {
            countNumVariantChoice--;
          } else {
            countNumVariantChoice++;
          }

          $this.find('.b-profile__amount-num').html(countNumVariantChoice);

          if ($this.find('.b-profile__amount-num').html() >= 1) {
            $this.find('.b-profile__amount-pick').show();
          }
        });
      });
    });

    if ($('.b-profile__item-wrap--concreteness .b-profile__amount-num').html() == 0) {
      $('.b-profile__item-wrap--concreteness .b-profile__amount-pick').hide();
    }

    $('.b-profile__item-wrap--concreteness .b-profile__item-inner').each(function () {
      var countNumConcreteness = 0,
          $this = $(this);
      $(this).find('.b-profile__input-checkbox-circle').each(function () {
        $(this).on('click', function () {
          if ($(this).hasClass('checked')) {
            countNumConcreteness--;
          } else {
            countNumConcreteness++;
          }

          $this.find('.b-profile__amount-num').html(countNumConcreteness);

          if ($this.find('.b-profile__amount-num').html() >= 1) {
            $this.find('.b-profile__amount-pick').show();
          }
        });
      });
    }); // $('.js-inner-val').on('focus', function () {
    //     $('.b-profile__list-selection').fadeIn();
    // });
    // $(document).on('click', function (e) {
    //     if ($(e.target).closest('.b-profile__metro-body').length) {
    //         return;
    //     }
    //     $('.b-profile__list-selection').fadeOut();
    // });
    //Выборка метро ("Не имеет значения")

    $('.b-profile__never-mind').on('click', function () {
      $('.b-profile__item-wrap--metro .b-profile__item-inner').fadeToggle();
      $('.b-profile__list-selection li').removeClass('select-list');
      $('.b-profile__station-list').removeClass('is-active-station-list');
    }); //Выборка метро (из списка, и отображение станций)

    $('.b-profile__list-selection--all-station').on('click', 'li', function () {
      var $thisSelectElem = $(this);
      $thisSelectElem.toggleClass('select-list');

      if ($('.b-profile__list-selection--all-station li').hasClass('select-list')) {
        $('.b-profile__station .b-profile__metro-title').fadeIn();
        $('.b-profile__station-name').removeClass('is-hide');
      } else {
        $('.b-profile__station .b-profile__metro-title').fadeOut();
      }

      if ($thisSelectElem.hasClass('select-list')) {
        $('.js-station-list').eq($thisSelectElem.index()).addClass('is-active-station-list');
      } else {
        $('.js-station-list').eq($thisSelectElem.index()).removeClass('is-active-station-list');
      }
    });
    $('.js-station-list .b-profile__station-name').on('click', function () {
      var $thisElem = $(this),
          DataId = $thisElem.data('id');
      $thisElem.addClass('is-hide');
      var $stationName = $('.b-profile__station-name[data-id="' + DataId + '"]').not('.is-hide');

      if (!$stationName.length) {
        $('.b-profile__list-selection--all-station li[data-id="' + DataId + '"]').removeClass('select-list');
        $('.b-profile__station-name[data-id="' + DataId + '"]').closest('.js-station-list').removeClass('is-active-station-list');
      }
    }); //Выборка метро (из списка)

    $('.b-profile__list-selection--single-station').on('click', 'li', function () {
      var $thisSelectElem = $(this);
      $thisSelectElem.toggleClass('select-list');

      if ($('.b-profile__list-selection--single-station li').hasClass('select-list')) {
        $('.b-profile__station .b-profile__metro-title').fadeIn();
        $('.b-profile__station-name').removeClass('is-hide');
      } else {
        $('.b-profile__station .b-profile__metro-title').fadeOut();
      }

      if ($thisSelectElem.hasClass('select-list')) {
        $('.js-station-list').eq($thisSelectElem.index()).addClass('is-active-station-list');
      } else {
        $('.js-station-list').eq($thisSelectElem.index()).removeClass('is-active-station-list');
      }
    });
    $('.js-station-list .b-profile__station-name').on('click', function () {
      var $thisElem = $(this),
          DataId = $thisElem.data('id');
      $thisElem.addClass('is-hide');
      var $stationName = $('.b-profile__station-name[data-id="' + DataId + '"]').not('.is-hide');

      if (!$stationName.length) {
        $('.b-profile__list-selection--single-station li[data-id="' + DataId + '"]').removeClass('select-list');
        $('.b-profile__station-name[data-id="' + DataId + '"]').closest('.js-station-list').removeClass('is-active-station-list');
      }
    }); //Автокомплит для метро

    $("#autocomplete-metro").keyup(function () {
      var $value = $(this).val();
      $(this).addClass('is-active-press');

      if ($(this).val() == '') {
        $(this).removeClass('is-active-press');
      }

      $(".js-autocomplete-metro li").each(function () {
        var $this = $(this);
        var $thisHtml = $(this).html();
        var reg = new RegExp($value, "i");

        if ($thisHtml.match(reg)) {
          $this.fadeIn();
        } else {
          $this.fadeOut();
        }
      });
    });
    $('.b-profile__item-inner').on('click', '.b-profile__input-radio', function () {
      $(this).parent().toggleClass('item-inner-is-active');
      $(this).siblings().stop().slideToggle().toggleClass('addit-content-show');
    });
    $('.b-profile__item-detail').on('click', '.b-profile__input-radio', function () {
      $(this).toggleClass('checked');
      /* Для радиокнопок */
      // $(this).addClass('checked');
      // $(this).siblings().removeClass('checked');
      // $(this).closest('.b-profile__item-inner').siblings().find('.b-profile__input-radio').removeClass('checked');
    });
    $('.b-profile__item-inner').on('click', '.b-profile__input-checkbox-circle', function () {
      $(this).toggleClass('checked');
    }); //Оценка выбора

    $('.b-profile__label-choice').on('click', function () {
      $(this).addClass('checked').siblings().removeClass('checked');
    });
    $('.b-profile__btn-remove').on('click', function () {
      $(this).closest('.b-profile__name').parent().addClass('choice-off');
    });
    $('.b-profile__btn-restore').on('click', function () {
      $(this).closest('.b-profile__assessment-choice-wrapp').parent().removeClass('choice-off');
    });
  }

  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  lightbox.option({
    'resizeDuration': 200,
    'showImageNumberLabel': false,
    'wrapAround': true
  });
  $(window).scroll(function () {
    if ($(window).scrollTop() > 600) {
      $('.fixed-btn-consultation').addClass('show');
    } else {
      $('.fixed-btn-consultation').removeClass('show');
    }
  });
});